# README #
###
This repository is intended to present to you the basics (and common extras) of writing e-dictionary of electrical terms using the Win32 API. 
The language used is C, most C++ compilers will compile it as well. 

As a matter of fact, most of the information is applicable to any language that can access the API, inlcuding Java, Assembly and Visual Basic.

This repository will not teach you the C language, nor will it tell you how to run your particular compiler (Borland C++, Visual C++, LCC-Win32, etc...) 

I will however take a few moments in the appendix to provide some notes on using the compilers I have knowledge of. 
###

* Quick Summary:

E-dictionary electrical terms provides you different terms in various categories. 

You can save a lot of time and effort through this electrical dictionary instead of searching for the terms on internet or marking the words in book.

Study as per your convenience without any charges.

Aspiring to become an electrical engineer?

Applying for the best electrical engineering schools?

e-dictionary electrical terms will make your preparation easy.

A repository of around 3400+ terms around electrical engineering in various customized categories.

My e-dictionary electrical terms is the app to have.

Now no need to Google nor turn countless pages to find a term and its meaning. Electrical engineering is effortless with e-dictionary electrical terms. All you need to do is search for terms and get its meaning.

* Version 01
* [Learn Programming](https://sa.linkedin.com/in/nedimbasic)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact